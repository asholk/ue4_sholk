// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SHOLKGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOLK_API ASHOLKGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
